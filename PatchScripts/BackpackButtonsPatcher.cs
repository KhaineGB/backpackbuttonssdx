﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using SDX.Core;
using SDX.Compiler;
using SDX.Payload;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System.Linq;
//using System.Reflection;


public class BackpackButtonsPatcher : IPatcherMod
{
    static bool bEnableNewUiButtons = true;

    public bool Patch(ModuleDefinition module)
    {
        FieldDefinition field;
        TypeDefinition gameClass;
        MethodDefinition gameMethod;
        TypeReference voidTypeRef = module.TypeSystem.Void;

        Console.WriteLine("==Backpack Butons Patcher===");

        if (bEnableNewUiButtons)
        {
            // Making required things public
            var xuiControllerTypeDef = module.Types.First(d => d.Name == "XUiController");
			
            gameClass = module.Types.First(d => d.Name == "XUiController");
            field = gameClass.Fields.First(d => d.Name == "viewComponent");
            SetFieldToPublic(field);
			
            gameClass = module.Types.First(d => d.Name == "XUiC_ItemStackGrid");
            field = gameClass.Fields.First(d => d.Name == "items");
            SetFieldToPublic(field);
            field = gameClass.Fields.First(d => d.Name == "itemControllers");
            SetFieldToPublic(field);
			
            gameClass = module.Types.First(d => d.Name == "XUiC_ItemStack");
            field = gameClass.Fields.First(d => d.Name == "placeSound");
            SetFieldToPublic(field);
        }

        return true;
    }
	
    // Helper functions to allow us to access and change variables that are otherwise unavailable.
    private void SetMethodToVirtual(MethodDefinition meth)
    {
        meth.IsVirtual = true;
    }

    private void SetFieldToPublic(FieldDefinition field)
    {
        field.IsFamily = false;
        field.IsPrivate = false;
        field.IsPublic = true;

    }
    private void SetMethodToPublic(MethodDefinition field)
    {
        field.IsFamily = false;
        field.IsPrivate = false;
        field.IsPublic = true;

    }

    // Called after the patching process and after scripts are compiled.
    // Used to link references between both assemblies
    // Return true if successful
    public bool Link(ModuleDefinition gameModule, ModuleDefinition modModule)
    {
        return true;
    }

}

