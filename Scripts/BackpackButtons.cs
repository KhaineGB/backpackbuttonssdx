﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System.IO;
using System.Xml;
using SDX.Core;
using System;

class XUiC_BackpackWindowSDX : XUiC_BackpackWindow
{
    static bool showDebugLog = false;

	// These designate the names of the buttons for the XUi controller
	private XUiController btnStashAll;
	private XUiController btnStashAllButFirst;
	private XUiController btnStashMatch;

    public static void DebugMsg(string msg)
    {
        if (showDebugLog)
        {
            UnityEngine.Debug.Log(msg);
        }
    }
	
	public static bool AddItem(ItemStack _itemStack, XUi _xui)
	{
		if (_xui.lootContainer == null)
		{
			return false;
		}
		_xui.lootContainer.TryStackItem(0, _itemStack);
		return _itemStack.count > 0 && _xui.lootContainer.AddItem(_itemStack);
	}
	
	// This sets up the buttons to actually work when poked. Also required so they can be called by XML
    public override void Init()
    {
        base.Init();
        this.btnStashAll = base.GetChildById("btnStashAll");
        this.btnStashAll.OnPress += this.ButtonStashAll_OnPressed;
        this.btnStashAllButFirst = base.GetChildById("btnStashAllButFirst");
        this.btnStashAllButFirst.OnPress += this.ButtonStashAllButFirst_OnPressed;
		this.btnStashMatch = base.GetChildById("btnStashMatch");
        this.btnStashMatch.OnPress += this.ButtonStashMatch_OnPressed;
	}
	
	// And this is all the code required to make the buttons actually DO something
    public void ButtonStashAll_OnPressed(XUiController _sender, OnPressEventArgs _onPressEventArgs)
    {
        XUiC_ItemStackGrid xuiC_ItemStackGrid = (XUiC_ItemStackGrid)base.GetChildByType<XUiC_ItemStackGrid>();
        XUiController[] slots = xuiC_ItemStackGrid.itemControllers;
        for (int i = 0; i < slots.Length; i++)
        {
            {
				// HandleMoveToPreferredLocation is a dick and likes to override sound settings. Code before and after it is to make placeSound shut the fuck up
				XUiC_ItemStack xuic_ItemStack = (XUiC_ItemStack)slots[i];
				var sound = xuic_ItemStack.placeSound; // Stores the original value of placeSound
				xuic_ItemStack.placeSound = null; // Sets placeSound to null so we don't have a RIP eardrums moment
				xuic_ItemStack.HandleMoveToPreferredLocation();
				xuic_ItemStack.placeSound = sound; // Reverts placeSound back to the stored value to undo our temp change
            }
        }
    }
	
    public void ButtonStashAllButFirst_OnPressed(XUiController _sender, OnPressEventArgs _onPressEventArgs)
    {
        XUiC_ItemStackGrid xuiC_ItemStackGrid = (XUiC_ItemStackGrid)base.GetChildByType<XUiC_ItemStackGrid>();
        XUiController[] slots = xuiC_ItemStackGrid.itemControllers;
        XUiV_Grid xuiv_Grid = (XUiV_Grid)xuiC_ItemStackGrid.viewComponent;
        for (int i = xuiv_Grid.Columns; i < slots.Length; i++)
        {
            {
				XUiC_ItemStack xuic_ItemStack = (XUiC_ItemStack)slots[i];
				var sound = xuic_ItemStack.placeSound;
				xuic_ItemStack.placeSound = null;
				xuic_ItemStack.HandleMoveToPreferredLocation();
				xuic_ItemStack.placeSound = sound;
            }
        }
    }

    public void ButtonStashMatch_OnPressed(XUiController _sender, OnPressEventArgs _onPressEventArgs)
    {
        ItemStack[] backpackslots = this.xui.PlayerInventory.GetBackpackItemStacks();
        XUiC_ItemStackGrid xuiC_ItemStackGrid = (XUiC_ItemStackGrid)base.GetChildByType<XUiC_ItemStackGrid>();
        XUiController[] slots = xuiC_ItemStackGrid.itemControllers;
        for (int i = 0; i < slots.Length; i++)
        {
            if (this.xui.lootContainer.HasItem(backpackslots[i].itemValue))
            {
				XUiC_ItemStack xuic_ItemStack = (XUiC_ItemStack)slots[i];
				var sound = xuic_ItemStack.placeSound;
				xuic_ItemStack.placeSound = null;
				xuic_ItemStack.HandleMoveToPreferredLocation();
				xuic_ItemStack.placeSound = sound;
			}
        }
    }
}	